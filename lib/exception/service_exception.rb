class ServiceException < Exception
  attr_reader :error_code
  attr_reader :message
  attr_reader :success

  ERRORS_TYPES = {
    :transfer_troubles => { :message => "Transport troubles", :code => 100 },
    :access_denied => { :message => "Access denied for user", :code => 101 },
    :not_authorized => { :message => "Account is not authorized", :code => 102 },
    :unauthorized => { :message => "Account is not authorized", :code => 401 },
    :forbidden => { :message => "This action is forbidden", :code => 403 },
    :invalid_service_arguments => { :message => "Invalid service arguments", :code => 103 },
    :record_not_found => { :message => "Record not found on the server", :code => 104 },
    :not_found => { :message => "Record not found on the server", :code => 404 },
    :failed_sign_in => { :message => "Invalid email or password", :code => 105 },
    :internal_service_error => { :message => "Internal server error", :code => 500 },
  }

  def to_json
    {:successful => @success, :code => @error_code, :message => @message}
  end
end

class TransportServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:transfer_troubles][:code], msg || ERRORS_TYPES[:transfer_troubles][:message]
    super
  end
end

class AccessDeniedServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:access_denied][:code], msg || ERRORS_TYPES[:access_denied][:message]
    super
  end
end

class UnAuthorizeServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:not_authorized][:code], msg || ERRORS_TYPES[:not_authorized][:message]
    super
  end
end

class UnAuthorizeException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:unauthorized][:code], msg || ERRORS_TYPES[:unauthorized][:message]
    super
  end
end

class ArgumentsServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:invalid_service_arguments][:code], msg || ERRORS_TYPES[:invalid_service_arguments][:message]
    super
  end
end

class FailedCredentialsException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:failed_sign_in][:code], msg || ERRORS_TYPES[:failed_sign_in][:message]
    super
  end
end

class RecordNotFoundServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:record_not_found][:code], msg || ERRORS_TYPES[:record_not_found][:message]
    super
  end
end

class NotFoundServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:not_found][:code], msg || ERRORS_TYPES[:not_found][:message]
    super
  end
end

class InternalServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:internal_service_error][:code], msg || ERRORS_TYPES[:internal_service_error][:message]
    super
  end
end

class ForbiddenServiceException < ServiceException
  def initialize(msg = nil)
    @success, @error_code, @message = false, ERRORS_TYPES[:forbidden][:code], msg || ERRORS_TYPES[:forbidden][:message]
    super
  end
end

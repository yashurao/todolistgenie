class Task < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :presence => true
  validate :check_task

  before_save :default_values

  def check_task
    unless new_record?
      if user_id_changed?
        errors.add(:user_id, 'cannot be changed.')
      end
      if completed_changed? && !completed
        errors.add(:completed, 'cannot be changed.')
      end
    end
  end

  def default_values
    self.completed ||= 0
  end
end

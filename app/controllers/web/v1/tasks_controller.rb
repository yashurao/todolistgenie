class Web::V1::TasksController < Web::V1::BaseController
  respond_to :json

  def index
    if params[:completed].present? && params[:completed].to_i == 0
      tasks = Task.where(completed: false)
    elsif params[:completed].present? && params[:completed].to_i == 1
      tasks = Task.where(completed: true)
    else
      tasks = Task.all
    end
    result = result_hash(true, 200, task: tasks)
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def show
    task =  Task.where(id: params[:id]).first
    if task
      result = result_hash(true, 200, task: task)
    else
      result = result_hash(false, 404, error: t('task.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def create
    user = User.find_by_id(task_params[:user_id])
    raise ArgumentsServiceException, "user_id, task parameters are absent" unless task_params[:user_id] || task_params[:text]
    raise ArgumentsServiceException, "User does not exists." unless user
    @task = Task.create(task_params)

    if @task.save
      result = result_hash(true, 200, task: @task)
    else
      result = result_hash(false, 403, error: @task.errors)
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def update
    task = Task.find_by_id(params[:id])
    if task
      # task.update_attributes(task: task_params[:task], completed: task_params[:completed])
      task.update_attributes!(task_params)
      result = result_hash(true, 200, task: task)
    else
      result = result_hash(false, 404, error: t('task.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def destroy
    task = Task.find_by_id(params[:id])
    if task
      if task.destroy
        result = result_hash(true, 200, message: t('task.destroySucess'))
      else
        result = result_hash(false, 404, error: t('task.notDelete'))
      end
    else
      result = result_hash(false, 405, error: t('task.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  private

  def task_params
    params.require(:task).permit(:user_id, :task, :completed) if params[:task]
  end
end

class Web::V1::UsersController < Web::V1::BaseController

  respond_to :json

  def index
    users = User.all
    result = result_hash(true, 200, user: users)
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def show
    user =  User.where(id: params[:id]).first
    if user
      result = result_hash(true, 200, user: user)
    else
      result = result_hash(false, 404, error: t('user.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def create
    @user = User.create(user_params)

    if @user.save
      result = result_hash(true, 200, user: @user)
    else
      result = result_hash(false, 403, error: @user.errors)
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def update
    user = User.find_by_id(params[:id])
    if user
      user.update_attributes(user_params)
      result = result_hash(true, 200, user: user)
    else
      result = result_hash(false, 404, error: t('user.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  def destroy
    user = User.find_by_id(params[:id])
    if user
      if user.destroy
        result = result_hash(true, 200, user: user, message: t('user.destroySucess'))
      else
        result = result_hash(false, 404, error: t('user.notDelete'))
      end
    else
      result = result_hash(false, 405, error: t('user.notFound'))
    end
  rescue ServiceException => ex
    result = ex.to_json
  rescue Exception => ex
    result = InternalServiceException.new("#{ex.try(:message)}").to_json
  ensure
    respond_to do |format|
      status = result[:code]
      result = result.except(:code)
      format.json { render :json => result, status: status }
      format.xml { render :xml => result, status: status }
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password) if params[:user]
  end
end

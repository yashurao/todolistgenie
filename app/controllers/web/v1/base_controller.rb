class Web::V1::BaseController < ApplicationController

  skip_before_filter :verify_authenticity_token
  # EXCLUDED_RESULT_PARAMS_FROM_RESULT_OBJECT = [:auth_token]

  helper_method :generate_multiple_information, :generate_result_hash

  # def generate_multiple_information(information_method_name, objects_scope)
  #   objects_array = []
  #   objects_scope.each do |object|
  #     objects_array << self.send(information_method_name, object)
  #   end
  #   objects_array
  # end
  #
  # def generate_result_hash(result_bool, options = {})
  #   result = {}
  #   result["successful"] = result_bool
  #   options.each do|k,v|
  #     # if EXCLUDED_RESULT_PARAMS_FROM_RESULT_OBJECT.include?(k)
  #       result[k] = v
  #     # else
  #     #   result[k] = v
  #     # end
  #   end
  #   result
  # end

  def result_hash(result_bool, code, options = {})
    result = {}
    result["successful"] = result_bool
    result[:code] = code
    options.each do|k,v|
      result[k] = v
    end
    result
  end

end

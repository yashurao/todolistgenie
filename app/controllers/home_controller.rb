class HomeController < ApplicationController

  def error
    render json: {success: false, message: 'Not Found'}, status: :not_found
  end
end

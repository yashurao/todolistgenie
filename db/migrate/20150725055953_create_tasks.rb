class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :user
      t.text :task
      t.boolean :completed

      t.timestamps null: false
    end
  end
end
